//require("dotenv").config({path: "../.env"});
const express = require("express");
const cors = require("cors");
const sql = require("mssql");
const app = express();
const sqlQuery = require("./sql_server/sqlQuery");
const PORT = 5000;

// console.log(process.env.DB_USER);

// TODO 1.env config
const config = (database) => {
  return {
    user: "eauser",
    password: "Foxconn88@ea",
    server: "ea-sqlserver-hh.database.windows.net",
    port: 1433,
    database: database,
    authentication: {
      type: "default",
    },
    options: {
      encrypt: true,
    },
  };
};

// const config = (database) => {
//  return {
//    user: JSON.stringfy(process.env.DB_USER),
//    password: JSON.stringfy(process.env.DB_PASS),
//    server: JSON.stringfy(process.env.DB_HOST),
//    port: 1433,
//    database: database,
//    authentication: {
//      type: "default",
//    },
//    options: {
//      encrypt: true,
//    },
//  };
// };

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

/**
 * Get all radar data
 */
app.get("/api/radartype", async (req, res) => {
  try {
    const result = await executeQuery(sqlQuery.radartype.query);
    res.json(result);
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
});

/**
 * Get radar data by radar type
 */
app.get("/api/hypecyclelist", async (req, res) => {
  try {
    const result = await executeQuery(sqlQuery.hypecyclelist.query);
    res.json(result);
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
});

/**
 * Get innovation tech data by hypecycle id
 */
app.get("/api/innovationtechs", async (req, res) => {
  try {
    const innovationIndex = req.query.innovationIndex;
    if (!innovationIndex) {
      return res.status(400).json({ error: "innovationIndex is required" });
    }
    const poolConnection = await sql.connect(config("ea_db_radar"));
    const resultSet = await poolConnection
      .request()
      .query(
        `SELECT * FROM [dbo].[radar_innovation_tech_data] WHERE id IN ${innovationIndex};`
      );
    res.json(resultSet.recordset);
    poolConnection.close();
  } catch (err) {
    console.error(err.message);
    res.status(500).json({ error: "Internal server error" });
  }
});

// TODO: 參數化（以下寫錯）
// app.get("/api/innovationtechs", async (req, res) => {
//   console.log(JSON.stringify(req.query.id));
//   const id = req.query.id;
//   const params = [{ name: "id", type: sql.Int, value: id }];
//   try {
//     const result = await executeParamsQuery(
//       sqlQuery.innovationtechs.query,
//       params
//     );
//     res.json(result);
//   } catch (err) {
//     res.status(500).json({ error: err.message });
//   }
// });

/**
 * submit survey result
 */
app.post("/api/submit", async (req, res) => {
  const { staff_id, staff_name_tw, staff_unit_name_tw, type_index, question_reply_content } =
    req.body;
  const params = [
    { name: "staff_id", type: sql.NChar, value: staff_id },
    { name: "staff_name_tw", type: sql.NVarChar, value: staff_name_tw },
    { name: "staff_unit_name_tw", type: sql.NVarChar, value: staff_unit_name_tw },
    { name: "type_index", type: sql.NChar, value: type_index },
    {
      name: "question_reply_content",
      type: sql.NChar,
      value: question_reply_content,
    },
  ];
  try {
    const result = await executeParamsQuery(sqlQuery.submit.query, params);
    res.json(result);
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
});

app.get("/api/test", async (req, res) => {
  const staff_id = req.query.staff_id;
  const params = [{ name: "staff_id", type: sql.NVarChar, value: staff_id }];
  try {
    const result = await executeParamsQuery(sqlQuery.test.query, params);
    res.json(result);
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
});

app.get("/api/init", (req, res) => {
  res.json({ message: "Hello from backend" });
});

app.post("/api/init", (req, res) => {
  res.status(201).send("Data received");
});

app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});

/**
 * Parameterized Queries Template
 * @param {string} sqlQuery
 * @param {any} params
 */
async function executeParamsQuery(sqlQuery, params) {
  try {
    const poolConnection = await sql.connect(config("ea_db_radar"));
    const request = poolConnection.request();
    params.forEach((param) => {
      request.input(param.name, param.type, param.value);
    });
    const resultSet = await request.query(sqlQuery);
    poolConnection.close();
    return resultSet.recordset;
  } catch (err) {
    console.error(err.message);
    throw new Error("Internal server error");
  }
}

/**
 * normal Queries Template
 * @param {string} sqlQuery
 */
async function executeQuery(sqlQuery) {
  try {
    const poolConnection = await sql.connect(config("ea_db_radar"));
    const request = poolConnection.request();
    const resultSet = await request.query(sqlQuery);
    poolConnection.close();
    return resultSet.recordset;
  } catch (err) {
    console.error(err.message);
    throw new Error("Internal server error");
  }
}
