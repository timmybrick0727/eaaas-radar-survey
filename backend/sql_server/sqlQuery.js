const sqlQuery = {
  test: {
    query: `SELECT * FROM [dbo].[radar_user_info] WHERE staff_id = @staff_id`,
  },
  radartype: {
    query: `SELECT * FROM [dbo].[radar_type_data]`,
  },
  hypecyclelist: {
    query: `SELECT * FROM [dbo].[radar_hype_cycle_data] ORDER BY title`,
  },
  innovationtechs: {
    query: `SELECT * FROM [dbo].[radar_innovation_tech_data] WHERE id = @id;`,
  },
  submit: {
    query: `INSERT INTO [dbo].[radar_survey_reply] (staff_id, staff_name_tw, staff_unit_name_tw, type_index, question_reply_content) VALUES (@staff_id, @staff_name_tw, @staff_unit_name_tw, @type_index, @question_reply_content);`,
  },
};

module.exports = sqlQuery;
