import React, { useState, useEffect } from "react";

const p = process.env.REACT_APP_API_URL;

console.log(p);

const url = `${process.env.REACT_APP_API_URL}/submit`; // 確保這裡的URL指向您的伺服器地址

function App() {
  const [data, setdata] = useState("");
  useEffect(() => {
    const testJSONString = JSON.stringify({
      testData: {
        innovation_tech_id: "id",
        utilizatio_level: "1",
        familiarity_level: "3",
      },
    });
    const data = {
      staff_id: "26908",
      staff_name_tw: "陳紀廷",
      type_index: "H001",
      question_reply_content: testJSONString,
    };

    fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((response) => {
        console.log("response", response);
        setdata(JSON.stringify(response));
        return response;
      })
      .then((data) => console.log(data));
  }, []);

  return (
    <div className="App">
      <button>測試撈取</button>
    </div>
  );
}

export default App;
