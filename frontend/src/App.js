// todo 游標
// todo 64 編號 名稱

import React, { useState, useEffect, useRef } from "react";
// import { Document, Page } from 'react-pdf';
import DropdownMenu from "./components/Dropdown";
import imgHypeCycle from "./images/790920_hypecycle.jpg";
import cover from "./images/img_cover.png";
import SVGRadarComponent from "./components/SVGRadarComponent";
// import MyStep from "./components/MyStep";
import "./App.css";
import {
  filterKeys,
  // extractTechIds,
  extractTechIds_new,
  DivButton,
  HypeCycleListBlock,
  InnovationListBlock,
  transformInput,
  transformFinallResult,
} from "./components/CosWheel";
import InputTextLimit from "./components/InputTextLimit";
import uiContent from "./content/uiContent.json";
import { HYPECYCLES } from "./content/hypeCycles";

// TODO: 2023.12.08 待加入環境變數,現在的寫法怪怪的不知為何?
const REACT_APP_API_URL =
  process.env.REACT_APP_API_URL || "http://20.28.192.182:5001/api";
// const REACT_APP_API_URL = "http://20.28.192.182:5001/api";

const activeStep = 1;

function App() {
  const [upload, setUpload] = useState(false);
  const [card1, setCard1] = useState(true);
  const [card2, setCard2] = useState(false);
  const [card3, setCard3] = useState(false);
  const [card4, setCard4] = useState(false);
  const [card5, setCard5] = useState(false);
  const [card6, setCard6] = useState(false);
  const [over, setOver] = useState(false);
  const [zone, setZone] = useState("");
  const [resultZone, setResultZone] = useState("");
  const [hypeCycleSelect, setHypeCycleSelect] = useState(0);
  const [hypeCycleSelectCard4, setHypeCycleSelectCard4] = useState(0);
  const [checkedState, setCheckedState] = useState({ 0: false });
  const [surveyResult, setSurveyResult] = useState([]);
  const [allResult, setAllResult] = useState([]);
  const [gartnerTitles, setGartnerTitles] = useState([]);
  const [gartnerIndex, setGartnerIndex] = useState([]);
  const [innovationIndexs, setInnovationIndexs] = useState([]);
  const [innovationAllData, setInnovationAllData] = useState([]);
  const [selectedValues, setSelectedValues] = useState([]);
  const [mediaQuery, setMediaQuery] = useState(230); // 預設值設為 230
  const [activeStep, setActiveStep] = useState(0);
  const [nowCard, setnowCard] = useState(0);
  const [next, setnext] = useState(false);
  const [nextRef, setnextRef] = useState(false);
  const [sqlHypecycle, setSqlHypecycle] = useState([
    { gartner_index: "", title: "", abstract_en: "" },
  ]);
  const [questionLength, setQuestionLength] = useState(0);

  const Card1Intro = ({ title, onClick }) => {
    const [name, setName] = useState("");
    const [num, setNum] = useState("");
    const [isValid, setIsValid] = useState({
      isValidNumber: false,
      isValidText: false,
      isWithinLimitNum: true,
      isWithinLimitText: true,
    });
    useEffect(() => {
      setName(localStorage.getItem("staff_name_tw") || "");
      setNum(localStorage.getItem("staffId") || "");
      setIsValid(
        JSON.parse(localStorage.getItem("isValid")) || {
          isValidNumber: false,
          isValidText: false,
          isWithinLimitNum: true,
          isWithinLimitText: true,
        },
      );
    }, []);
    return (
      <>
        <div className="flex flex-row">
          <div className="text-2xl md:text-3xl font-bold">{title}</div>
          {/* <div className="text-4xl font-bold">{<MyStep />}</div> */}
        </div>
        <div
          style={{ overflowY: "auto", overflowX: "hidden" }}
          className="flex flex-1 p-4 flex-col md:flex-row justify-center gap-3 sm:gap-12"
        >
          <div className="flex flex-1 md:flex-[3] flex-col justify-center gap-6 text-lg">
            <div className="flex mt-3">{uiContent.step1.description}</div>
            {/* 圖片 */}
            <div className="hidden md:flex flex-1 items-center justify-center">
              {/* <img className="sm:h-[50vh] md:h-[30vh] sm:w-[50vh] md:w-[50vh]" src={cover} alt="描述" /> */}
              <img className="w-[60rem] sm:w-[70%]" src={cover} alt="描述" />
            </div>
          </div>
          <div className="flex flex-[3] items-center justify-center">
            <div
              style={{
                border: "1.5px solid rgba(0,0,0,0.1)",
                width: "100%",
                height: "80%",
              }}
              className="flex flex-col rounded-lg p-6 sm:p-12 md:p-6 items-start justify-between shadow-md"
            >
              <div
                style={{
                  fontWeight: "revert",
                }}
                className="text-xl md:text-2xl mb-3"
              >
                {"請填入基本資訊"}
              </div>
              <div className="flex flex-row items-center justify-between md:justify-start w-full">
                <div className="text-base md:text-base">
                  <div>{`所屬單位：`}</div>
                  {/* <div>{`${resultZone}`}</div> */}
                </div>
                <DropdownMenu
                  list={uiContent.step1.class}
                  setResultZone={setResultZone}
                  resultZone={resultZone}
                />
              </div>
              <InputTextLimit
                key={"staff_name_tw"}
                title={"staff_name_tw"}
                titleDisplay={"姓名"}
                limit={10}
                state={name}
                setState={setName}
                isValid={isValid}
                setIsValid={setIsValid}
                type={"text"}
              />
              <InputTextLimit
                key={"staffId"}
                title={"staffId"}
                titleDisplay={"工號"}
                limit={10}
                state={num}
                setState={setNum}
                isValid={isValid}
                setIsValid={setIsValid}
                type={"number"}
              />
            </div>
          </div>
        </div>
        <div className="flex justify-end gap-6">
          <DivButton
            bgColor={
              isValid.isValidNumber &&
              isValid.isValidText &&
              isValid.isWithinLimitNum &&
              isValid.isWithinLimitText &&
              num !== "" &&
              name !== "" &&
              resultZone !== ""
                ? "#007bff"
                : "gray"
            }
            text={"下一步"}
            onClick={onClick}
            noPoint={
              isValid.isValidNumber &&
              isValid.isValidText &&
              isValid.isWithinLimitNum &&
              isValid.isWithinLimitText
                ? ""
                : "none"
            }
          />
        </div>
      </>
    );
  };

  const Card2ChoiceClass = ({ title, lastClick, nextClick, zone }) => {
    return (
      <>
        <div className="text-2xl md:text-3xl font-bold mb-1">{title}</div>
        <div className="flex flex-1 md:flex-[2] md:px-10 flex-col md:flex-row">
          <div
            style={{ minWidth: "300px" }}
            className="flex flex-[1] md:flex-[3] flex-col justify-center text-xl gap-3 mt-3"
            // css
          >
            <div className="mb-1 text-bold text-base sm:text-2xl lg:3xl">
              {uiContent.step2.description["1"]}
            </div>
            <div
              className="pl-6 sm:flex flex-col gap-2 hidden text-sm md:text-lg"
              // style={{ fontSize: "clamp(12px, 1.5vw, 24px)" }}
            >
              <div>{uiContent.step2.description["2"]}</div>
              <div>{uiContent.step2.description["3"]}</div>
              <div>{uiContent.step2.description["4"]}</div>
              <div>{uiContent.step2.description["5"]}</div>
            </div>
          </div>
          <div
            style={{ overflowY: "auto" }}
            className="flex flex-[4] items-center flex-col justify-center"
          >
            <div className="bg-gray-200 md:bg-none md:hidden w-full h-[1px] my-2"></div>
            <div className="text-base sm:text-3xl font-medium">
              請「點擊」下方雷達圖選擇對應分類：
            </div>
            <div
              style={{
                position: "relative",
                cursor: "pointer",
              }}
            >
              <div
                className="text-xl sm:text-4xl"
                style={{
                  position: "absolute",
                  zIndex: 2,
                  left: "15%",
                  top: "25%",
                  fontWeight: "bolder",
                  color: "white",
                  cursor: "pointer",
                }}
                onClick={() => {
                  setZone("H001");
                  setResultZone("H001");
                }}
              >
                創新研發
              </div>
              <div
                className="text-xl sm:text-4xl"
                style={{
                  position: "absolute",
                  zIndex: 2,
                  left: "55%",
                  top: "25%",
                  fontWeight: "bolder",
                  color: "white",
                  cursor: "pointer",
                }}
                onClick={() => {
                  setZone("H004");
                  setResultZone("H004");
                }}
              >
                雲網服務
              </div>
              <div
                className="text-xl sm:text-4xl"
                style={{
                  position: "absolute",
                  zIndex: 2,
                  left: "15%",
                  top: "65%",
                  fontWeight: "bolder",
                  color: "white",
                  cursor: "pointer",
                }}
                onClick={() => {
                  setZone("H002");
                  setResultZone("H002");
                }}
              >
                應用開發
              </div>
              <div
                className="text-xl sm:text-4xl"
                style={{
                  position: "absolute",
                  zIndex: 2,
                  left: "55%",
                  top: "65%",
                  fontWeight: "bolder",
                  color: "white",
                  cursor: "pointer",
                }}
                onClick={() => {
                  setZone("H003");
                  setResultZone("H003");
                }}
              >
                系統運維
              </div>
              <SVGRadarComponent
                radius={mediaQuery}
                zone={zone}
                setZone={setZone}
                setResultZone={setResultZone}
              />
            </div>
          </div>
        </div>
        <div className="flex justify-end gap-6">
          <DivButton text={"上一步"} onClick={lastClick} />
          <DivButton
            bgColor={zone == "" ? "gray" : "#007bff"}
            noPoint={zone == "" ? "none" : ""}
            text={"下一步"}
            onClick={nextClick}
          />
        </div>
      </>
    );
  };

  const Card5Submit = ({ title, lastClick, nextClick }) => {
    return (
      <>
        <div className="text-2xl font-bold">{title}</div>
        <div className="flex-1 p-4 flex flex-col h-3">
          <div
            className="flex flex-1 flex-col"
            style={{
              overflowY: "auto",
              borderLeft: "1.5px solid rgba(0,0,0,0.5)",
              borderRight: "1.5px solid rgba(0,0,0,0.5)",
              borderTop: "1.5px solid rgba(0,0,0,0.5)",
              borderBottom: "1.5px solid rgba(0,0,0,0.5)",
            }}
          >
            {gartnerTitles &&
              gartnerTitles.map((item, index) => {
                return (
                  <InnovationListBlock
                    key={index}
                    HypeCycleName={
                      filterKeysByValue(checkedState)[index] + 1 + ". " + item
                    }
                    onClick={() => {
                      setHypeCycleSelectCard4(index);
                    }}
                    backgroundColor={
                      index === hypeCycleSelectCard4 ? "rgba(0,0,0,0.2)" : ""
                    }
                  />
                );
              })}
          </div>
          <div
            className="flex flex-[3] flex-col p-4 bg-neutral-200 gap-3 text-xs"
            style={{
              overflowY: "auto",
              borderLeft: "1.5px solid rgba(0,0,0,0.5)",
              borderBottom: "1.5px solid rgba(0,0,0,0.5)",
              borderRight: "1.5px solid rgba(0,0,0,0.5)",
              borderTop: "1.5px solid rgba(0,0,0,0.5)",
            }}
            key={"checkScroll"}
          >
            {/* {JSON.stringify(innovationIndexs[innovationSelect])} */}
            {innovationAllData[hypeCycleSelectCard4] &&
              innovationAllData[hypeCycleSelectCard4]["TechList"].map(
                (item, index) => {
                  // console.log(index, "##########");
                  // setCurrentTechName(item["innovation_tech_name"]);
                  return (
                    <div className="flex flex-col rounded-lg bg-slate-100 shadow-sm p-4">
                      <div className="text-xl flex">
                        {item["innovation_tech_name"]}
                      </div>
                      <div style={{ flex: 2 }} className="flex flex-col">
                        <div
                          style={{ backgroundColor: "rgba(0,0,0,0.1)" }}
                          className="flex flex-1 justify-between"
                        >
                          <div
                            className="flex flex-1 items-center justify-center flex-col gap-2"
                            style={{
                              fontSize: "clamp(12px, 1.5vw, 24px)",
                              fontWeight: "normal",
                              borderTop: "1.3px solid rgba(0,0,0,0.5)",
                              borderBottom: "1.3px solid rgba(0,0,0,0.5)",
                              borderLeft: "1.3px solid rgba(0,0,0,0.5)",
                              borderRight: "1.3px solid rgba(0,0,0,0.5)",
                            }}
                          >
                            <div>可行性</div>
                            <div>評分</div>
                          </div>
                          <div
                            className="flex-1 flex flex-col items-center justify-center p-1"
                            style={{
                              borderTop: "1.3px solid rgba(0,0,0,0.5)",
                              borderBottom: "1.3px solid rgba(0,0,0,0.5)",
                              borderRight: "1.3px solid rgba(0,0,0,0.5)",
                            }}
                          >
                            <div className="tesxt-sm">1 分</div>
                            <div className="flex items-center justify-center">
                              <input
                                type="radio"
                                name={
                                  gartnerIndex[hypeCycleSelectCard4] +
                                  item["innovation_tech_name"]
                                }
                                checked={
                                  selectedValues?.[
                                    gartnerIndex[hypeCycleSelectCard4] +
                                      item["innovation_tech_name"]
                                  ]?.["tech_assement"]?.[
                                    "feasibility_level"
                                  ] === "1"
                                }
                                onChange={() => {
                                  /**
                                   * ! this is final result
                                   * ! this is final result
                                   * ! this is final result
                                   */
                                  setSelectedValues({
                                    ...selectedValues,
                                    [gartnerIndex[hypeCycleSelectCard4] +
                                    item["innovation_tech_name"]]: {
                                      gartner_index:
                                        gartnerIndex[hypeCycleSelectCard4],
                                      tech_assement: {
                                        tech_id: String(item["id"]),
                                        feasibility_level: "1",
                                        // useage_level: "",
                                      },
                                    },
                                  });
                                }}
                                value="1"
                                style={{
                                  transform: "scale(2)",
                                  margin: "10px",
                                }}
                                disabled={card5 ? true : false}
                              />
                            </div>
                          </div>
                          <div
                            className="flex-1 flex flex-col items-center justify-center p-1"
                            style={{
                              borderTop: "1.3px solid rgba(0,0,0,0.5)",
                              borderBottom: "1.3px solid rgba(0,0,0,0.5)",
                              borderRight: "1.3px solid rgba(0,0,0,0.5)",
                            }}
                          >
                            <div className="tesxt-sm">2 分</div>
                            <div className="flex items-center justify-center">
                              <input
                                type="radio"
                                name={
                                  gartnerIndex[hypeCycleSelectCard4] +
                                  item["innovation_tech_name"]
                                }
                                checked={
                                  selectedValues?.[
                                    gartnerIndex[hypeCycleSelectCard4] +
                                      item["innovation_tech_name"]
                                  ]?.["tech_assement"]?.[
                                    "feasibility_level"
                                  ] === "2"
                                }
                                onChange={() => {
                                  /**
                                   * ! this is final result
                                   * ! this is final result
                                   * ! this is final result
                                   */
                                  setSelectedValues({
                                    ...selectedValues,
                                    [gartnerIndex[hypeCycleSelectCard4] +
                                    item["innovation_tech_name"]]: {
                                      gartner_index:
                                        gartnerIndex[hypeCycleSelectCard4],
                                      tech_assement: {
                                        tech_id: String(item["id"]),
                                        feasibility_level: "2",
                                        // useage_level: "",
                                      },
                                    },
                                  });
                                }}
                                value="2"
                                style={{
                                  transform: "scale(2)",
                                  margin: "10px",
                                }}
                                disabled={card5 ? true : false}
                              />
                            </div>
                          </div>
                          <div
                            className="flex-1 flex flex-col items-center justify-center p-1"
                            style={{
                              borderTop: "1.3px solid rgba(0,0,0,0.5)",
                              borderBottom: "1.3px solid rgba(0,0,0,0.5)",
                              borderRight: "1.3px solid rgba(0,0,0,0.5)",
                            }}
                          >
                            <div className="tesxt-sm">3 分</div>
                            <div className="flex items-center justify-center">
                              <input
                                type="radio"
                                name={
                                  gartnerIndex[hypeCycleSelectCard4] +
                                  item["innovation_tech_name"]
                                }
                                checked={
                                  selectedValues?.[
                                    gartnerIndex[hypeCycleSelectCard4] +
                                      item["innovation_tech_name"]
                                  ]?.["tech_assement"]?.[
                                    "feasibility_level"
                                  ] === "3"
                                }
                                onChange={() => {
                                  /**
                                   * ! this is final result
                                   * ! this is final result
                                   * ! this is final result
                                   */
                                  setSelectedValues({
                                    ...selectedValues,
                                    [gartnerIndex[hypeCycleSelectCard4] +
                                    item["innovation_tech_name"]]: {
                                      gartner_index:
                                        gartnerIndex[hypeCycleSelectCard4],
                                      tech_assement: {
                                        tech_id: String(item["id"]),
                                        feasibility_level: "3",
                                        // useage_level: "",
                                      },
                                    },
                                  });
                                }}
                                value="3"
                                style={{
                                  transform: "scale(2)",
                                  margin: "10px",
                                }}
                                disabled={card5 ? true : false}
                              />
                            </div>
                          </div>
                          <div
                            className="flex-1 flex flex-col items-center justify-center p-1"
                            style={{
                              borderTop: "1.3px solid rgba(0,0,0,0.5)",
                              borderBottom: "1.3px solid rgba(0,0,0,0.5)",
                              borderRight: "1.3px solid rgba(0,0,0,0.5)",
                            }}
                          >
                            <div className="tesxt-sm">4 分</div>
                            <div className="flex items-center justify-center">
                              <input
                                type="radio"
                                name={
                                  gartnerIndex[hypeCycleSelectCard4] +
                                  item["innovation_tech_name"]
                                }
                                checked={
                                  selectedValues?.[
                                    gartnerIndex[hypeCycleSelectCard4] +
                                      item["innovation_tech_name"]
                                  ]?.["tech_assement"]?.[
                                    "feasibility_level"
                                  ] === "4"
                                }
                                onChange={() => {
                                  /**
                                   * ! this is final result
                                   * ! this is final result
                                   * ! this is final result
                                   */
                                  setSelectedValues({
                                    ...selectedValues,
                                    [gartnerIndex[hypeCycleSelectCard4] +
                                    item["innovation_tech_name"]]: {
                                      gartner_index:
                                        gartnerIndex[hypeCycleSelectCard4],
                                      tech_assement: {
                                        tech_id: String(item["id"]),
                                        feasibility_level: "4",
                                        // useage_level: "",
                                      },
                                    },
                                  });
                                }}
                                value="4"
                                style={{
                                  transform: "scale(2)",
                                  margin: "10px",
                                }}
                                disabled={card5 ? true : false}
                              />
                            </div>
                          </div>
                          <div
                            className="flex-1 flex flex-col items-center justify-center p-1"
                            style={{
                              borderTop: "1.3px solid rgba(0,0,0,0.5)",
                              borderBottom: "1.3px solid rgba(0,0,0,0.5)",
                              borderRight: "1.3px solid rgba(0,0,0,0.5)",
                            }}
                          >
                            <div className="tesxt-sm">5 分</div>
                            <div className="flex items-center justify-center">
                              <input
                                type="radio"
                                name={
                                  gartnerIndex[hypeCycleSelectCard4] +
                                  item["innovation_tech_name"]
                                }
                                checked={
                                  selectedValues?.[
                                    gartnerIndex[hypeCycleSelectCard4] +
                                      item["innovation_tech_name"]
                                  ]?.["tech_assement"]?.[
                                    "feasibility_level"
                                  ] === "5"
                                }
                                onChange={() => {
                                  /**
                                   * ! this is final result
                                   * ! this is final result
                                   * ! this is final result
                                   */
                                  setSelectedValues({
                                    ...selectedValues,
                                    [gartnerIndex[hypeCycleSelectCard4] +
                                    item["innovation_tech_name"]]: {
                                      gartner_index:
                                        gartnerIndex[hypeCycleSelectCard4],
                                      tech_assement: {
                                        tech_id: String(item["id"]),
                                        feasibility_level: "5",
                                        // useage_level: "",
                                      },
                                    },
                                  });
                                }}
                                value="5"
                                style={{
                                  transform: "scale(2)",
                                  margin: "10px",
                                }}
                                disabled={card5 ? true : false}
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  );
                },
              )}
          </div>
        </div>
        <div className="flex justify-end gap-6">
          <DivButton text={"上一步"} onClick={lastClick} />
          <DivButton
            text={"送出"}
            onClick={() => {
              console.log(
                transformFinallResult(selectedValues),
                "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!",
              );
              setOver(true);
            }}
          />
        </div>
        {/* {upload && <div style={{back}} className="w-screen h-screen"></div>} */}
      </>
    );
  };

  /**
   * Check the size of the window and set the media query accordingly.
   */
  useEffect(() => {
    const checkSize = () => {
      if (window.innerWidth < 640) {
        setMediaQuery(120);
      } else if (window.innerWidth < 1024) {
        setMediaQuery(170); // 其他情況，設置 radius 為 230
      } else if (window.innerWidth < 1280) {
        setMediaQuery(300); // 其他情況，設置 radius 為 230
      }
    };
    window.addEventListener("resize", checkSize);
    checkSize();
    return () => window.removeEventListener("resize", checkSize);
  }, []);

  useEffect(() => {
    setAllResult((pre) => [...pre, surveyResult]);
  }, [surveyResult]);

  useEffect(() => {
    if (card2) {
      fetch(`${REACT_APP_API_URL}/radartype`)
        .then((response) => response.json())
        .then((result) => {
          console.log(result);
          // setRadarType(result);
        })
        .catch((error) => {
          console.error("Error fetching data:", error);
        });
    }
  }, [card2]);

  useEffect(() => {
    if (card3) {
      fetch(`${REACT_APP_API_URL}/hypecyclelist`)
        .then((response) => response.json())
        .then((result) => {
          console.log(result);
          setSqlHypecycle(result); // 將獲取的數據設置到狀態中
        })
        .catch((error) => {
          console.error("Error fetching data:", error);
        });
    }
  }, [card3]);

  useEffect(() => {
    console.log(questionLength, "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
  }, [questionLength]);

  const divRefs = useRef([]);
  useEffect(() => {
    divRefs.current = innovationAllData.map(
      (item, index) => divRefs.current[index] || React.createRef(),
    );
  }, [innovationAllData]);

  const scrollToDiv = (index) => {
    const currentDiv = divRefs.current[index];
    if (currentDiv) {
      currentDiv.scrollIntoView({ behavior: "smooth" });
    }
  };

  useEffect(() => {
    scrollToDiv(nextRef);
  }, [next]);

  function filterKeysByValue(input) {
    const output = [];

    for (const key in input) {
      if (input[key]) {
        output.push(parseInt(key));
      }
    }

    return output;
  }

  return (
    <div className="flex-col h-screen bg-slate-500 flex items-center justify-center">
      {/* <MyStep curStep={nowCard} /> */}
      <div
        // style={{ width: "90%", height: "90%" }}
        className="flex flex-col bg-slate-50 rounded-xl p-6 gap-2 shadow-2xl w-full h-full sm:w-[90%] sm:h-[90%] justify-between"
      >
        {card1 && (
          <Card1Intro
            title={uiContent.step1.title}
            onClick={() => {
              setCard1(false);
              setCard2(false);
              setCard3(true);
              setCard4(false);
              setnowCard(3);
            }}
          />
        )}
        {card2 && (
          <Card2ChoiceClass
            title={uiContent.step2.title}
            lastClick={() => {
              setCard1(true);
              setCard2(false);
              setCard3(false);
              setCard4(false);
              setZone("");
            }}
            nextClick={() => {
              setCard1(false);
              setCard2(false);
              setCard3(true);
              setCard4(false);
              setnowCard(3);
            }}
            zone={zone}
          />
        )}
        {card3 && (
          <>
            <div className="text-2xl font-bold">{"專家評估-技術分類"}</div>
            <div
              style={{ minWidth: "300px" }}
              className="flex flex-col justify-center text-sm md:text-xl mt-3"
              // css
            >
              <div>{uiContent.step3.description["1"]}</div>
            </div>
            <div className="flex flex-col md:flex-row h-3/4 border-[2px] border-black">
              <div
                className="border-black border-b-[0.5px] md:border-b-[0] md:border-r-[0.5px]"
                style={{
                  flex: 1,
                  overflowY: "auto",
                }}
              >
                {sqlHypecycle &&
                  sqlHypecycle.map((item, index) => {
                    // console.log(item);
                    return (
                      <HypeCycleListBlock
                        key={index}
                        hypeCycleTitle={item.title}
                        onClick={() => {
                          setHypeCycleSelect(index);
                        }}
                        backgroundColor={
                          index === hypeCycleSelect ? "rgba(0,0,0,0.2)" : ""
                        }
                        id={index}
                        checkedState={checkedState}
                        setCheckedState={setCheckedState}
                      />
                    );
                  })}
              </div>
              <div
                className="flex flex-[3] flex-col p-4 bg-neutral-200 gap-3 text-sm"
                style={{
                  overflowY: "auto",
                }}
              >
                <div className="md:text-xl font-bold">
                  {sqlHypecycle[hypeCycleSelect]["title"]}
                </div>
                {/* <div>{sqlHypecycle[hypeCycleSelect]["gartner_index"]}</div> */}
                <div class="flex flex-col sm:flex-row gap-2 mb-6">
                  <div className="flex flex-1 flex-col">
                    <div className="text-base">{"英文摘要"}</div>

                    <div>{sqlHypecycle[hypeCycleSelect]["abstract_en"]}</div>
                  </div>
                  <div className="flex flex-1 flex-col">
                    <div className="text-base">{"中文摘要"}</div>
                    <div>{sqlHypecycle[hypeCycleSelect]["abstract_tw"]}</div>
                  </div>
                </div>

                {/* <div>{sqlHypecycle[hypeCycleSelect]["releaseDate"]}</div> */}
                <img
                  className="-mb-3 md:-mb-6"
                  style={{ clipPath: "inset(0 0 6% 0)" }}
                  src={
                    HYPECYCLES[sqlHypecycle[hypeCycleSelect]["gartner_index"]]
                      ?.url
                  }
                  alt={
                    HYPECYCLES[sqlHypecycle[hypeCycleSelect]["gartner_index"]]
                      ?.title
                  }
                />
                <img
                  style={{ clipPath: "inset(0 0 6px 0)" }}
                  src={
                    HYPECYCLES[
                      sqlHypecycle[hypeCycleSelect]["gartner_index"] + "_extra"
                    ]?.url
                  }
                  alt={
                    HYPECYCLES[
                      sqlHypecycle[hypeCycleSelect]["gartner_index"] + "_extra"
                    ]?.title
                  }
                />
              </div>
            </div>
            <div className="flex justify-end gap-6">
              {/* {"測試：" + JSON.stringify(filterKeys(checkedState))} */}
              <DivButton
                text={"上一步"}
                onClick={() => {
                  setCard1(true);
                  setCard2(false);
                  setCard3(false);
                  setCard4(false);
                }}
              />
              <DivButton
                bgColor={
                  filterKeys(checkedState).length > 0 ? "#007bff" : "gray"
                }
                noPoint={filterKeys(checkedState).length > 0 ? "" : "none"}
                text={"下一步"}
                onClick={() => {
                  let idex = [];
                  let tempGartnerTitles = [];
                  let tempGartnerIndex = [];

                  filterKeys(checkedState).forEach((item) => {
                    tempGartnerTitles.push(sqlHypecycle[item]["title"]);
                    tempGartnerIndex.push(sqlHypecycle[item]["gartner_index"]);
                    idex.push(
                      extractTechIds_new(
                        JSON.parse(
                          sqlHypecycle[item]["use_tech_list_json_all"],
                        ),
                      ),
                    );
                  });

                  setGartnerTitles(tempGartnerTitles);
                  setGartnerIndex(tempGartnerIndex);
                  setInnovationIndexs(idex);

                  const arrayToString = (arr) => {
                    return `(${arr.join(",")})`;
                  };

                  const fetchPromises = idex.map((innovationIds, index) => {
                    return fetch(
                      `${REACT_APP_API_URL}/innovationtechs?innovationIndex=${arrayToString(
                        innovationIds,
                      )}`,
                    )
                      .then((response) => response.json())
                      .then((result) => {
                        let techAssementList = {};
                        result.forEach((item) => {
                          techAssementList[item["id"]] = {
                            utilizatio_level: 0,
                            feasibility_level: 0,
                          };
                        });

                        return {
                          HypeCycleName: tempGartnerTitles[index],
                          gartner_index: tempGartnerIndex[index],
                          tech_assement_list: techAssementList,
                          TechList: result,
                        };
                      })
                      .catch((error) => {
                        console.error("Error fetching data:", error);
                        return null;
                      });
                  });

                  Promise.all(fetchPromises).then((allData) => {
                    const validData = allData.filter((data) => data !== null);
                    setInnovationAllData(validData);
                    setQuestionLength(
                      validData.reduce(
                        (total, data) => total + data.TechList.length,
                        0,
                      ),
                    );
                  });

                  setCard1(false);
                  setCard2(false);
                  setCard3(false);
                  setCard4(true);
                  setnowCard(4);
                }}
              />
            </div>
          </>
        )}
        {card4 && (
          <>
            <div className="text-2xl font-bold">{"專家評估-技術評分"}</div>
            <div
              style={{ minWidth: "300px" }}
              className="flex flex-col justify-center text-sm sm:text-xl mt-3"
              // css
            >
              <div>{uiContent.step4.description["1"]}</div>
            </div>
            <div className="flex-1 flex flex-col md:flex-row border-[2px] border-black h-3/5">
              <div
                className="border-black border-b-[0.5px] md:border-b-[0] md:border-r-[0.5px]"
                style={{
                  flex: 1,
                  overflowY: "auto",
                }}
              >
                {gartnerTitles &&
                  gartnerTitles.map((item, index) => {
                    return (
                      <InnovationListBlock
                        key={index}
                        HypeCycleName={
                          filterKeysByValue(checkedState)[index] +
                          1 +
                          ". " +
                          item
                        }
                        onClick={() => {
                          setHypeCycleSelectCard4(index);
                        }}
                        backgroundColor={
                          index === hypeCycleSelectCard4
                            ? "rgba(0,0,0,0.2)"
                            : ""
                        }
                      />
                    );
                  })}
              </div>
              <div
                className="flex flex-[3] flex-col p-4 bg-neutral-200 gap-3 text-sm"
                style={{
                  overflowY: "auto",
                }}
                key={"rightScroll"}
              >
                {innovationAllData[hypeCycleSelectCard4] &&
                  innovationAllData[hypeCycleSelectCard4]["TechList"].map(
                    (item, index) => {
                      return (
                        <div
                          ref={(el) =>
                            (divRefs.current[
                              gartnerIndex[hypeCycleSelectCard4] + `${index}`
                            ] = el)
                          }
                          className="flex flex-col rounded-lg bg-slate-100 shadow-sm p-4"
                        >
                          <div
                            style={{ flex: 5 }}
                            className="flex flex-col gap-6"
                          >
                            <div className="text-xl flex">
                              {item["innovation_tech_name"]}
                            </div>
                            <div className="text-base flex">
                              {item["definition"]}
                            </div>
                            <div className="text-lg flex flex-row gap-12 ">
                              <div>{item["maturity"]}</div>
                              <div>{item["position"]}</div>
                              <div>{item["time_to_plateau"]}</div>
                            </div>
                          </div>
                          <div style={{ flex: 2 }} className="flex flex-col">
                            <div
                              style={{ backgroundColor: "rgba(0,0,0,0.1)" }}
                              className="flex flex-1 justify-between"
                            >
                              <div
                                className="flex flex-1 items-center justify-center flex-col gap-2"
                                style={{
                                  fontSize: "clamp(12px, 1.5vw, 24px)",
                                  fontWeight: "normal",
                                  borderTop: "1.3px solid rgba(0,0,0,0.5)",
                                  borderBottom: "1.3px solid rgba(0,0,0,0.5)",
                                  borderLeft: "1.3px solid rgba(0,0,0,0.5)",
                                  borderRight: "1.3px solid rgba(0,0,0,0.5)",
                                }}
                              >
                                <div>可行性</div>
                                <div>評分</div>
                              </div>
                              <div
                                className="flex-1 flex flex-col items-center justify-center p-1"
                                style={{
                                  borderTop: "1.3px solid rgba(0,0,0,0.5)",
                                  borderBottom: "1.3px solid rgba(0,0,0,0.5)",
                                  borderRight: "1.3px solid rgba(0,0,0,0.5)",
                                }}
                              >
                                <div className="text-sm">1 分</div>
                                <div className="flex items-center justify-center">
                                  <input
                                    type="radio"
                                    name={
                                      gartnerIndex[hypeCycleSelectCard4] +
                                      item["innovation_tech_name"]
                                    }
                                    checked={
                                      selectedValues?.[
                                        gartnerIndex[hypeCycleSelectCard4] +
                                          item["innovation_tech_name"]
                                      ]?.["tech_assement"]?.[
                                        "feasibility_level"
                                      ] === "1"
                                    }
                                    onChange={() => {
                                      /**
                                       * ! this is final result
                                       * ! this is final result
                                       * ! this is final result
                                       */
                                      setSelectedValues({
                                        ...selectedValues,
                                        [gartnerIndex[hypeCycleSelectCard4] +
                                        item["innovation_tech_name"]]: {
                                          gartner_index:
                                            gartnerIndex[hypeCycleSelectCard4],
                                          tech_assement: {
                                            tech_id: String(item["id"]),
                                            feasibility_level: "1",
                                            // useage_level: "",
                                          },
                                        },
                                      });

                                      if (
                                        index + 1 <
                                        innovationAllData[hypeCycleSelectCard4][
                                          "TechList"
                                        ].length
                                      ) {
                                        console.log(
                                          gartnerIndex[hypeCycleSelectCard4] +
                                            `${index + 1}`,
                                          "~~~~~~~~~",
                                        );
                                        setnextRef(
                                          gartnerIndex[hypeCycleSelectCard4] +
                                            `${index + 1}`,
                                        );
                                        setnext(!next);
                                      } else {
                                        console.log(
                                          innovationAllData[
                                            hypeCycleSelectCard4
                                          ]["TechList"].length,
                                        );
                                        setnextRef(
                                          gartnerIndex[hypeCycleSelectCard4] +
                                            "0",
                                        );
                                        setnext(!next);
                                      }
                                    }}
                                    value="1"
                                    style={{
                                      transform: "scale(2)",
                                      margin: "10px",
                                    }}
                                    disabled={card5 ? true : false}
                                  />
                                </div>
                              </div>
                              <div
                                className="flex-1 flex flex-col items-center justify-center p-1"
                                style={{
                                  borderTop: "1.3px solid rgba(0,0,0,0.5)",
                                  borderBottom: "1.3px solid rgba(0,0,0,0.5)",
                                  borderRight: "1.3px solid rgba(0,0,0,0.5)",
                                }}
                              >
                                <div className="text-sm">2 分</div>
                                <div className="flex items-center justify-center">
                                  <input
                                    type="radio"
                                    name={
                                      gartnerIndex[hypeCycleSelectCard4] +
                                      item["innovation_tech_name"]
                                    }
                                    checked={
                                      selectedValues?.[
                                        gartnerIndex[hypeCycleSelectCard4] +
                                          item["innovation_tech_name"]
                                      ]?.["tech_assement"]?.[
                                        "feasibility_level"
                                      ] === "2"
                                    }
                                    onChange={() => {
                                      /**
                                       * ! this is final result
                                       * ! this is final result
                                       * ! this is final result
                                       */
                                      setSelectedValues({
                                        ...selectedValues,
                                        [gartnerIndex[hypeCycleSelectCard4] +
                                        item["innovation_tech_name"]]: {
                                          gartner_index:
                                            gartnerIndex[hypeCycleSelectCard4],
                                          tech_assement: {
                                            tech_id: String(item["id"]),
                                            feasibility_level: "2",
                                            // useage_level: "",
                                          },
                                        },
                                      });
                                      if (
                                        index + 1 <
                                        innovationAllData[hypeCycleSelectCard4][
                                          "TechList"
                                        ].length
                                      ) {
                                        console.log(
                                          gartnerIndex[hypeCycleSelectCard4] +
                                            `${index + 1}`,
                                          "~~~~~~~~~",
                                        );
                                        setnextRef(
                                          gartnerIndex[hypeCycleSelectCard4] +
                                            `${index + 1}`,
                                        );
                                        setnext(!next);
                                      } else {
                                        console.log(
                                          innovationAllData[
                                            hypeCycleSelectCard4
                                          ]["TechList"].length,
                                        );
                                        setnextRef(
                                          gartnerIndex[hypeCycleSelectCard4] +
                                            "0",
                                        );
                                        setnext(!next);
                                      }
                                    }}
                                    value="2"
                                    style={{
                                      transform: "scale(2)",
                                      margin: "10px",
                                    }}
                                    disabled={card5 ? true : false}
                                  />
                                </div>
                              </div>
                              <div
                                className="flex-1 flex flex-col items-center justify-center p-1"
                                style={{
                                  borderTop: "1.3px solid rgba(0,0,0,0.5)",
                                  borderBottom: "1.3px solid rgba(0,0,0,0.5)",
                                  borderRight: "1.3px solid rgba(0,0,0,0.5)",
                                }}
                              >
                                <div className="text-sm">3 分</div>
                                <div className="flex items-center justify-center">
                                  <input
                                    type="radio"
                                    name={
                                      gartnerIndex[hypeCycleSelectCard4] +
                                      item["innovation_tech_name"]
                                    }
                                    checked={
                                      selectedValues?.[
                                        gartnerIndex[hypeCycleSelectCard4] +
                                          item["innovation_tech_name"]
                                      ]?.["tech_assement"]?.[
                                        "feasibility_level"
                                      ] === "3"
                                    }
                                    onChange={() => {
                                      /**
                                       * ! this is final result
                                       * ! this is final result
                                       * ! this is final result
                                       */
                                      setSelectedValues({
                                        ...selectedValues,
                                        [gartnerIndex[hypeCycleSelectCard4] +
                                        item["innovation_tech_name"]]: {
                                          gartner_index:
                                            gartnerIndex[hypeCycleSelectCard4],
                                          tech_assement: {
                                            tech_id: String(item["id"]),
                                            feasibility_level: "3",
                                            // useage_level: "",
                                          },
                                        },
                                      });
                                      if (
                                        index + 1 <
                                        innovationAllData[hypeCycleSelectCard4][
                                          "TechList"
                                        ].length
                                      ) {
                                        console.log(
                                          gartnerIndex[hypeCycleSelectCard4] +
                                            `${index + 1}`,
                                          "~~~~~~~~~",
                                        );
                                        setnextRef(
                                          gartnerIndex[hypeCycleSelectCard4] +
                                            `${index + 1}`,
                                        );
                                        setnext(!next);
                                      } else {
                                        console.log(
                                          innovationAllData[
                                            hypeCycleSelectCard4
                                          ]["TechList"].length,
                                        );
                                        setnextRef(
                                          gartnerIndex[hypeCycleSelectCard4] +
                                            "0",
                                        );
                                        setnext(!next);
                                      }
                                    }}
                                    value="3"
                                    style={{
                                      transform: "scale(2)",
                                      margin: "10px",
                                    }}
                                    disabled={card5 ? true : false}
                                  />
                                </div>
                              </div>
                              <div
                                className="flex-1 flex flex-col items-center justify-center p-1"
                                style={{
                                  borderTop: "1.3px solid rgba(0,0,0,0.5)",
                                  borderBottom: "1.3px solid rgba(0,0,0,0.5)",
                                  borderRight: "1.3px solid rgba(0,0,0,0.5)",
                                }}
                              >
                                <div className="text-sm">4 分</div>
                                <div className="flex items-center justify-center">
                                  <input
                                    type="radio"
                                    name={
                                      gartnerIndex[hypeCycleSelectCard4] +
                                      item["innovation_tech_name"]
                                    }
                                    checked={
                                      selectedValues?.[
                                        gartnerIndex[hypeCycleSelectCard4] +
                                          item["innovation_tech_name"]
                                      ]?.["tech_assement"]?.[
                                        "feasibility_level"
                                      ] === "4"
                                    }
                                    onChange={() => {
                                      /**
                                       * ! this is final result
                                       * ! this is final result
                                       * ! this is final result
                                       */
                                      setSelectedValues({
                                        ...selectedValues,
                                        [gartnerIndex[hypeCycleSelectCard4] +
                                        item["innovation_tech_name"]]: {
                                          gartner_index:
                                            gartnerIndex[hypeCycleSelectCard4],
                                          tech_assement: {
                                            tech_id: String(item["id"]),
                                            feasibility_level: "4",
                                            // useage_level: "",
                                          },
                                        },
                                      });
                                      if (
                                        index + 1 <
                                        innovationAllData[hypeCycleSelectCard4][
                                          "TechList"
                                        ].length
                                      ) {
                                        console.log(
                                          gartnerIndex[hypeCycleSelectCard4] +
                                            `${index + 1}`,
                                          "~~~~~~~~~",
                                        );
                                        setnextRef(
                                          gartnerIndex[hypeCycleSelectCard4] +
                                            `${index + 1}`,
                                        );
                                        setnext(!next);
                                      } else {
                                        console.log(
                                          innovationAllData[
                                            hypeCycleSelectCard4
                                          ]["TechList"].length,
                                        );
                                        setnextRef(
                                          gartnerIndex[hypeCycleSelectCard4] +
                                            "0",
                                        );
                                        setnext(!next);
                                      }
                                    }}
                                    value="4"
                                    style={{
                                      transform: "scale(2)",
                                      margin: "10px",
                                    }}
                                    disabled={card5 ? true : false}
                                  />
                                </div>
                              </div>
                              <div
                                className="flex-1 flex flex-col items-center justify-center p-1"
                                style={{
                                  borderTop: "1.3px solid rgba(0,0,0,0.5)",
                                  borderBottom: "1.3px solid rgba(0,0,0,0.5)",
                                  borderRight: "1.3px solid rgba(0,0,0,0.5)",
                                }}
                              >
                                <div className="text-sm">5 分</div>
                                <div className="flex items-center justify-center">
                                  <input
                                    type="radio"
                                    className="cursor-pointer"
                                    name={
                                      gartnerIndex[hypeCycleSelectCard4] +
                                      item["innovation_tech_name"]
                                    }
                                    checked={
                                      selectedValues?.[
                                        gartnerIndex[hypeCycleSelectCard4] +
                                          item["innovation_tech_name"]
                                      ]?.["tech_assement"]?.[
                                        "feasibility_level"
                                      ] === "5"
                                    }
                                    onChange={() => {
                                      /**
                                       * ! this is final result
                                       * ! this is final result
                                       * ! this is final result
                                       */
                                      setSelectedValues({
                                        ...selectedValues,
                                        [gartnerIndex[hypeCycleSelectCard4] +
                                        item["innovation_tech_name"]]: {
                                          gartner_index:
                                            gartnerIndex[hypeCycleSelectCard4],
                                          tech_assement: {
                                            tech_id: String(item["id"]),
                                            feasibility_level: "5",
                                            // useage_level: "",
                                          },
                                        },
                                      });
                                      if (
                                        index + 1 <
                                        innovationAllData[hypeCycleSelectCard4][
                                          "TechList"
                                        ].length
                                      ) {
                                        console.log(
                                          gartnerIndex[hypeCycleSelectCard4] +
                                            `${index + 1}`,
                                          "~~~~~~~~~",
                                        );
                                        setnextRef(
                                          gartnerIndex[hypeCycleSelectCard4] +
                                            `${index + 1}`,
                                        );
                                        setnext(!next);
                                      } else {
                                        console.log(
                                          innovationAllData[
                                            hypeCycleSelectCard4
                                          ]["TechList"].length,
                                        );
                                        setnextRef(
                                          gartnerIndex[hypeCycleSelectCard4] +
                                            "0",
                                        );
                                        setnext(!next);
                                      }
                                    }}
                                    value="5"
                                    style={{
                                      transform: "scale(2)",
                                      margin: "10px",
                                    }}
                                    disabled={card5 ? true : false}
                                  />
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      );
                    },
                  )}
              </div>
            </div>
            <div className="flex justify-end gap-6 items-center flex-row">
              <div className="w-[130px] sm:w-[300px] h-[20px] bg-gray-300 rounded-full">
                <div
                  className={`h-[20px] relative bg-gray-400 rounded-full`}
                  style={{
                    width: `${
                      (Number(Object.keys(selectedValues).length) /
                        Number(questionLength)) *
                      100
                    }%`,
                  }}
                ></div>
              </div>
              <div className="grid grid-cols-2 gap-3 sm:gap-6">
                <DivButton
                  text={"上一步"}
                  onClick={() => {
                    setCard1(false);
                    setCard2(false);
                    setCard3(true);
                    setCard4(false);
                    setCard5(false);
                    setGartnerTitles([]);
                    setSelectedValues({});
                    setGartnerIndex([]);
                    setInnovationIndexs([]);
                    setInnovationAllData([]);
                    setQuestionLength(0);
                  }}
                />
                {/* {JSON.stringify(Object.keys(selectedValues).length)}
              {JSON.stringify(questionLength)} */}
                <DivButton
                  bgColor={
                    Object.keys(selectedValues).length == questionLength
                      ? "#007bff"
                      : "gray"
                  }
                  noPoint={
                    Object.keys(selectedValues).length == questionLength
                      ? ""
                      : "none"
                  }
                  text={"下一步"}
                  onClick={() => {
                    setCard1(false);
                    setCard2(false);
                    setCard3(false);
                    setCard4(false);
                    setCard5(true);
                    setnowCard(4);
                    transformInput(selectedValues);
                  }}
                />
              </div>
              {/* {"測試：" + JSON.stringify(transformInput(selectedValues))} */}
              {/* {Number(Object.keys(selectedValues).length)/Number(questionLength) * 100} */}
            </div>
          </>
        )}
        {card5 && (
          <Card5Submit
            title={uiContent.step5.title}
            lastClick={() => {
              setCard1(false);
              setCard2(false);
              setCard3(false);
              setCard4(true);
              setCard5(false);
              // setGartnerTitles([]);
              // setGartnerIndex([]);
              // setInnovationIndexs([]);
              // setQuestionLength(0);
            }}
            nextClick={() => {
              setCard1(false);
              setCard2(false);
              setCard3(false);
              setCard4(false);
              setCard5(false);
              setOver(true);
              setnowCard(4);
            }}
          />
        )}
        {card6 && (
          <div className="h-full flex justify-center items-center">
            {/* <div className="flex flex-row h-full">
                <div className="text-4xl font-bold">{}</div>
              </div> */}
            <div className="flex flex-col justify-between items-stretch h-3/6 w-3/7 px-6">
              <div className="text-xl md:text-3xl font-bold mb-3">
                {"感謝您的填寫！"}
              </div>
              <div className="text-xl font-bold mt-3">
                {uiContent.step5.description["1"]}
              </div>
              <div className="text-3xl font-bold">
                {uiContent.step5.description["2"]}
              </div>
              <div className="flex justify-end mt-6">
                <DivButton
                  // bgColor={name != "" && num != "" ? "" : "gray"}
                  text={"提交其他回應"}
                  onClick={() => {
                    window.location.reload();
                    // setCard1(true);
                    // setCard2(false);
                    // setCard3(false);
                    // setCard4(false);
                    // setCard5(false);
                    // setCard6(false);
                    // setnowCard(1);
                  }}
                />
              </div>
            </div>
          </div>
        )}
        {/* {over && <div style={{width:"100vw", height:"100vh", position:"relative", backgroundColor:"rgba(0,0,0,0.5)"}}> */}
        {over && (
          <div>
            <div
              style={{
                position: "fixed",
                top: "50%",
                left: "50%",
                transform: "translate(-50%, -50%)",
                backgroundColor: "rgba(0, 0, 0, 0.7)",
                padding: "20px",
                // borderRadius: "10px",
                boxShadow: "0 0 10px rgba(0, 0, 0, 0.5)",
                width: "100vw",
                height: "100vh",
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <div
                className="w-[60vw] md:w-[30vh]"
                style={{
                  backgroundColor: "white",
                  borderRadius: "10px",
                  padding: "20px",
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  justifyContent: "space-between",
                }}
              >
                {/* 在這裡添加您的內容 */}
                <div className="text-2xl mb-3">是否送出評估問卷？</div>
                <div className="flex gap-2">
                  <DivButton
                    text={"返回"}
                    onClick={() => {
                      setOver(false);
                    }}
                  />
                  <DivButton
                    text={"送出"}
                    onClick={() => {
                      console.log(
                        JSON.stringify({
                          staff_id: localStorage.getItem("staffId"),
                          staff_name_tw: localStorage.getItem("staff_name_tw"),
                          type_index: "H001",
                          staff_unit_name_tw: resultZone,
                          question_reply_content: JSON.stringify(
                            transformFinallResult(selectedValues),
                          ),
                        }),
                      );
                      fetch(REACT_APP_API_URL + "/submit", {
                        method: "POST",
                        headers: {
                          "Content-Type": "application/json",
                        },
                        body: JSON.stringify({
                          staff_id: localStorage.getItem("staffId"),
                          staff_name_tw: localStorage.getItem("staff_name_tw"),
                          type_index: "H001",
                          staff_unit_name_tw: resultZone,
                          question_reply_content: JSON.stringify(
                            transformFinallResult(selectedValues),
                          ),
                        }),
                      })
                        .then((response) => {
                          console.log("response", response);
                          setCard1(false);
                          setCard2(false);
                          setCard3(false);
                          setCard4(false);
                          setCard5(false);
                          setCard6(true);
                          setOver(false);
                          setnowCard(1);
                          return response;
                        })
                        .then((data) => {
                          console.log(data);
                          localStorage.removeItem("staffId");
                          localStorage.removeItem("staff_name_tw");
                          localStorage.removeItem("isValid");
                        });
                    }}
                  />
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    </div>
  );
}

export default App;
