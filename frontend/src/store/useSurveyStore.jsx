import { create } from "zustand";
import zukeeper from "zukeeper";

const useSurveyStore = create(
  zukeeper((set, get) => ({
    card: 0,
    userNumber: "",
    userName: "",
    zoneForCal: "",
    zoneForResult: "",
    mediaQueryToRadius: 150,
    apiRadarType: [],
    sqlHypecyclelist: [],
    checkedState: {},
    hypeCycleSelect: 0,
    handelLastCard: () =>
      set((state) => {
        if (state.card > 0) {
          return { card: state.card - 1 };
        } else {
          return { card: 4 };
        }
      }),
    handleNextCard: () =>
      set((state) => {
        if (state.card < 4) {
          return { card: state.card + 1 };
        } else {
          return { card: 4 };
        }
      }),
    setUserNumber: (number) => set({ userNumber: number }),
    setUserName: (name) => set({ userName: name }),
    setZoneForCal: (zone) => set({ zoneForCal: zone }),
    setZoneForResult: (zone) => set({ zoneForResult: zone }),
    setMediaQueryToRadius: (radius) => set({ mediaQueryToRadius: radius }),
    setSqlHypecyclelist: (data) => set({ sqlHypecycle: data }),
    setHypeCycleSelect: (index) => set({ hypeCycleSelect: index }),
    setCheckedState: (state) => set({ checkedState: state }),
    fetchRadarType: async () => {
      const response = await fetch(`${import.meta.env.VITE_API_URL}/radartype`);
      const data = await response.json();
      set({ apiRadarType: data });
      return data;
    },
    fetchSqlHypecyclelist: async () => {
      const response = await fetch(
        `${import.meta.env.VITE_API_URL}/hypecyclelist`,
      );
      const data = await response.json();
      set({ sqlHypecyclelist: data });
      return data;
    },
  })),
);
window.store = useSurveyStore;

export default useSurveyStore;
