import { useState, useEffect } from "react";

export default function InputTextLimit({
  title,
  titleDisplay,
  limit,
  state,
  setState,
  isValid,
  setIsValid,
  type,
}) {
  // 使用 useEffect 來處理輸入驗證的側效
  const [init, setInit] = useState(true);

  useEffect(() => {
    // 定義數字和中英文字的正則表達式
    // const regNumberAndLetters = /^[0-9]*$/;
    const regNumberAndLetters = /^[0-9A-Za-z-_]*$/;
    const regText = /^[a-zA-Z\u4e00-\u9fa5]+$/;

    // 若為空，則不進行驗證
    // console.log(state === "", "~~~~~~~~~~~~~~~~~~")
    if (!init && state !== "") {
      // 根據輸入類型進行驗證
      if (type === "number") {
        console.log("number", type, state, limit);
        console.log(regNumberAndLetters.test(state));
        setIsValid((pre) => ({
          ...pre,
          isValidNumber: regNumberAndLetters.test(state),
          isWithinLimitNum: state.length < limit,
        }));
      } else if (type === "text") {
        console.log("text", type, state, limit);
        setIsValid((pre) => ({
          ...pre,
          isValidText: regText.test(state),
          isWithinLimitText: state.length < limit,
        }));
      }
    }
  }, [state]);

  useEffect(() => {
    setIsValid((pre) => ({
      ...pre,
      isValidNumber: false,
      isValidText: false,
      isWithinLimitNum: true,
      isWithinLimitText: true,
    }));
  }, []);

  useEffect(() => {
    console.log(isValid);
  }, [isValid]);

  return (
    <>
      <div className="flex flex-row items-center">
        <div className="text-base md:text-xl">{`${titleDisplay}：`}</div>
        <div className="flex flex-col gap-1 items-center justify-center">
          {/* 根據輸入的類型和驗證結果顯示對應的錯誤訊息 */}
          {type === "number" &&
            isValid?.["isValidNumber"] !== true &&
            init === false && (
              <div className="text-red-500 text-sm">請輸入數字或英文</div>
            )}
          {type === "number" &&
            isValid?.["isWithinLimitNum"] !== true &&
            init === false && (
              <div className="text-red-500 text-sm">字數最多 {limit} 字</div>
            )}
          {type === "text" &&
            isValid?.["isValidText"] !== true &&
            init === false && (
              <div className="text-red-500 text-sm">請輸入中文或英文</div>
            )}
          {type === "text" &&
            isValid?.["isWithinLimitText"] !== true &&
            init === false && (
              <div className="text-red-500 text-sm">字數最多 {limit} 字</div>
            )}
        </div>
      </div>
      <input
        type={"text"}
        maxLength={limit}
        className="mb-3 bg-slate-200 w-full shadow-sm rounded-lg border-x-stone-300 p-2"
        // value={state}
        value={localStorage.getItem(title) || ""}
        onChange={(e) => {
          // 設置狀態並儲存到 localStorage
          setState(e.target.value);
          localStorage.setItem(title, e.target.value);
          localStorage.setItem("isValid", JSON.stringify(isValid));
          // 設置 init 為 false，表示已經輸入過了
          setInit(false);
        }}
      />
    </>
  );
}
